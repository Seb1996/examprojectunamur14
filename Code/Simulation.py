import pandas as pd
import numpy  as np
import random as rnd
import Clients
import time
import datetime
import matplotlib.pyplot as plt



show_simulation_report = True



######## IMPORT DATA
print('Importing Data: ...')

# Percentages
print('   Percentages ...')
Percentages = pd.read_csv('../Outputs/Exploratory/probabilities_Part_1.csv', sep=';')

# ExistingIDs
print('   CIDs ...')
available_CIDs = pd.read_csv('../Outputs/Exploratory/Available_IDs.csv', sep=';')

print('Importing Data: DONE \n')



########Defining Simulation Constants
# Daily visits
Num_returning = 1000
perc_returning = 20  # [%]
perc_one_time = 80  # [%]

Num_ret_hipsters = int(round(1 / 3 * Num_returning))
Num_ret_regular = int(round(2 / 3 * Num_returning))

Price_Increase = 0  # [%]

# Time to Simulate
start_year = 2020
years_to_simulate = 1



######## Product pricing
pricing_food = {'sandwich': 5 * (100 + Price_Increase) / 100,
                'muffin': 3 * (100 + Price_Increase) / 100,
                'pie': 3 * (100 + Price_Increase) / 100,
                'cookie': 2 * (100 + Price_Increase) / 100,
                'no order': 0 * (100 + Price_Increase) / 100}

pricing_drink = {'milkshake': 5 * (100 + Price_Increase) / 100,
                 'frappucino': 4 * (100 + Price_Increase) / 100,
                 'coffee': 3 * (100 + Price_Increase) / 100,
                 'soda': 3 * (100 + Price_Increase) / 100,
                 'tea': 3 * (100 + Price_Increase) / 100,
                 'water': 2 * (100 + Price_Increase) / 100}



######## MORE DATAHANDLING
Percentages_food = Percentages.ix[:, ['HOURS', 'cookie', 'muffin', 'pie', 'sandwich']]
Percentages_drinks = Percentages.ix[:, ['HOURS', 'coffee', 'frappucino', 'milkshake', 'soda', 'tea', 'water']]
Percentages_food['no order'] = 1 - Percentages_food.sum(axis=1)
available_IDs = available_CIDs['CLIENT_ID']
daily_times = Percentages['HOURS'].unique()



######## Preparing Simulation
print('Preparing Simulation Data: ...')

# Genarating initial client batch
returning_of_the_day = []
one_time_of_the_day = []
ID_index = 0


def create_returners(ID_index):
    returning_of_the_day = []
    for i in range(Num_returning):
        if i < Num_ret_hipsters:
            returning_of_the_day.append(Clients.ReturningHipster(available_IDs[ID_index]))
        else:
            returning_of_the_day.append(Clients.ReturningRegular(available_IDs[ID_index]))
        ID_index += 1

    rnd.shuffle(returning_of_the_day)
    return returning_of_the_day, ID_index


# Creating Batch of Returning Clients
returning_clients, ID_index = create_returners(ID_index)

# Generating easy to use percentages
day_probabilities = {}
for index, time_slot in enumerate(daily_times):
    # Selecting percentages per timeslot
    timeslot_percentages_food = Percentages_food.iloc[index, 1:]
    timeslot_percentages_drinks = Percentages_drinks.iloc[index, 1:]

    day_probabilities.update({time_slot: {'drink_options': list(timeslot_percentages_drinks.keys()),
                                          'drink_probs': [timeslot_percentages_drinks[x] for x in
                                                          timeslot_percentages_drinks.keys()],
                                          'food_options': list(timeslot_percentages_food.keys()),
                                          'food_probs': [timeslot_percentages_food[x] for x in
                                                         timeslot_percentages_food.keys()]
                                          }
                              });

# Generating Date Range
start_date = datetime.date(start_year, 1, 1)
end_date = datetime.date(start_year + (years_to_simulate + 1), 1, 1)
delta = end_date - start_date
date_list = [start_date + datetime.timedelta(days=x) for x in range(0, delta.days)]

print('Preparing Simulation Data: DONE \n')



######### Simulation
print('Running Simulation Data: ...')

# Creating history logs
logs = []
no_more_money = []
daily_incomes = []
incomes = []


# Selecting A Returning Customer from prepared batch
def select_returning(returning_clients, no_more_money):
    selected_client = returning_clients[rnd.randint(0, len(returning_clients) - 1)]

    while not selected_client.Does_have_enough_budget(10) and len(returning_clients) > 1:
        no_more_money.append(selected_client)
        returning_clients.remove(selected_client)
        selected_client = returning_clients[rnd.randint(0, len(returning_clients) - 1)]

    return selected_client, returning_clients, no_more_money


# Creating A One-timer Customer
def create_one_timer(client_id):
    client_type = np.random.choice(['trip-advisor', 'regular'], p=[0.1, 0.9])
    # client_id   = available_IDs[ID_index]

    if client_type == 'trip-advisor':
        one_timer = Clients.OnceTripAdvisor(client_id)
    else:
        one_timer = Clients.OnceRegular(client_id)

    return one_timer


# Generating Customer Order
def Order(drink_choices, drink_probabilities, food_choices, food_probabilities):
    drink = np.random.choice(drink_choices, p=drink_probabilities)
    food = np.random.choice(food_choices, p=food_probabilities)
    return drink, food


# Progress logging
start_time_sim = time.time()
start_time_progress = time.time()
old_time = start_date.year

for date in date_list:

    # Showing progress to user
    if date.year != old_time:
        print(str(old_time) + ':   ' + str(round(time.time() - start_time_progress, 2)) + ' seconds')
        old_time = date.year
        start_time_progress = time.time()
    old_time = date.year

    daily_income = 0
    for time_slot_index, time_slot in enumerate(daily_times):
        log_time = str(date) + ' ' + time_slot
        income = 0

        customer_type = np.random.choice(['returning', 'one-time'], p=[perc_returning / 100, perc_one_time / 100])

        # Selecting Customer Type
        if customer_type == 'returning' and len(returning_clients) > 0:

            if len(returning_clients) == 1 and returning_clients[0].Does_have_enough_budget(10):
                no_more_money.append(returning_clients[0])
                returning_clients = []

                client_id = available_IDs[ID_index]
                ID_index += 1
                client = create_one_timer(client_id)
                no_more_returning_customers_since = date

            else:
                client, returning_clients, no_more_money = select_returning(returning_clients, no_more_money)

        else:
            client_id = available_IDs[ID_index]
            ID_index += 1

            client = create_one_timer(client_id)

        # Generating Customer Order
        Client_order = Order(day_probabilities[time_slot]['drink_options'],
                             day_probabilities[time_slot]['drink_probs'],
                             day_probabilities[time_slot]['food_options'],
                             day_probabilities[time_slot]['food_probs'])
        costs = [pricing_drink[Client_order[0]], pricing_food[Client_order[1]]]

        # Customer payment
        client.Buy_drink(Client_order[0], costs[0], log_time)
        client.Buy_food(Client_order[1], costs[1], log_time)
        income += sum(costs)

        # Client Tip
        if sum(costs) > 0:
            given_tip = client.Give_tip(log_time)
            income += given_tip

        logs.append([log_time, client.id, Client_order[0], Client_order[1]])
        incomes.append([log_time, client.id, costs[0], costs[1], given_tip, income])
        daily_income += income

    daily_incomes.append(daily_income)
total_simulation_time = time.time() - start_time_sim

print('Running Simulation Data: DONE\n')
if show_simulation_report:
    print('-------------------------------------')
    print('    SIMULATION REPORT')
    print('-------------------------------------')
    print('Simulation_time: ', round(total_simulation_time, 5), 'seconds')
    print('Clients Created: ', "{:,}".format(ID_index))
    print('-------------------------------------')
    print('Total Income: ', '€ ' + "{:,}".format(round(sum(daily_incomes), 2)))
    print('"Returning Clients" out of money: ', "{:,}".format(len(no_more_money)), '\n')



######## Making Data frames from client order logs
for client in (no_more_money + returning_clients):
    client.drink_orders = pd.DataFrame.from_dict(client.drink_orders)
    client.food_orders = pd.DataFrame.from_dict(client.food_orders)
    client.budget_history = pd.DataFrame.from_dict(client.budget_history)



######## Creating Dataframes of logs
Logs = pd.DataFrame.from_records(logs, columns=['TIME', 'CLIENT ID', 'DRINKS', 'FOOD'])
Incomes = pd.DataFrame.from_records(incomes,
                                    columns=['TIME', 'CLIENT ID', 'PRICE DRINK', 'PRICE FOOD', 'TIP', 'INCOME'])

Logs['TIME'] = pd.to_datetime(Logs['TIME'])
Incomes['TIME'] = pd.to_datetime(Incomes['TIME'])

Incomes_grouped_by_time = Incomes.groupby([pd.Grouper(key='TIME', freq='M')])
Incomes_summed_by_time = pd.DataFrame()
Incomes_summed_by_time['PRICE DRINK'] = Incomes_grouped_by_time['PRICE DRINK'].sum()
Incomes_summed_by_time['PRICE FOOD'] = Incomes_grouped_by_time['PRICE FOOD'].sum()
Incomes_summed_by_time['TIP'] = Incomes_grouped_by_time['TIP'].sum()
Incomes_summed_by_time['INCOME'] = Incomes_grouped_by_time['INCOME'].sum()
Incomes_summed_by_time['TIME'] = Incomes_summed_by_time.index



######## Generating Simulation Percentages for comparison
Logs['HOURS'] = Logs['TIME'].dt.time
Logs['DATES'] = Logs['TIME'].dt.date

Number_unique_date = Logs['DATES'].nunique()

Percentages_drinks_sim = Logs[['HOURS', 'DRINKS']].groupby(
    ['HOURS', 'DRINKS']).size().unstack() / Number_unique_date  # * 100
Percentages_food_sim = Logs[['HOURS', 'FOOD']].groupby(['HOURS', 'FOOD']).size().unstack() / Number_unique_date  # * 100

frames = [Percentages_food_sim, Percentages_drinks_sim]
Percentages_sim = pd.concat(frames, axis=1)

Percentages_sim['HOURS'] = Percentages_sim.index
Percentages_sim = Percentages_sim.fillna(value=0)



######## Generating Plots
print('Preparing Plots: ...')

plt.plot(np.arange(len(Incomes_summed_by_time['INCOME'])), Incomes_summed_by_time['INCOME'], label='Total Income', lw=1)
plt.plot(np.arange(len(Incomes_summed_by_time['PRICE DRINK'])), Incomes_summed_by_time['PRICE DRINK'],
         label='Drinks Income', lw=1)
plt.plot(np.arange(len(Incomes_summed_by_time['PRICE FOOD'])), Incomes_summed_by_time['PRICE FOOD'],
         label='Food Income', lw=1)
plt.plot(np.arange(len(Incomes_summed_by_time['TIP'])), Incomes_summed_by_time['TIP'], label='Tip Income', lw=1)

plt.legend()
plt.ylim([0, 1.4 * np.max(Incomes_summed_by_time['INCOME'])])
plt.xticks(
    np.arange(0, len(Incomes_summed_by_time['INCOME']), len(Incomes_summed_by_time['INCOME']) / years_to_simulate),
    np.arange(start_year, start_year + years_to_simulate + 1),
    rotation=45,
    ha='center')

plt.show()

CoffeebarSimulation = pd.read_csv("../Outputs/Simulation/CoffeeBar_2018-01-01_2023-12-31.csv", sep=";")

foodFiveYears = True
if foodFiveYears:
    Food = CoffeebarSimulation['FOOD'].value_counts()

    Food.plot.bar(title='plot1 : Total amount of sold foods over the five years', color=['g'])
    plt.show()

    Food.plot.pie(autopct='%.2f', fontsize=10, figsize=(6, 6), title='Extra plot: Percentage of sold food over the five years', colors=['plum', 'blueviolet', 'deeppink', 'dodgerblue', 'm'])
    plt.show()

drinksFiveYears = True
if drinksFiveYears:
    Drinks = CoffeebarSimulation['DRINKS'].value_counts()

    Drinks.plot.bar(title='plot2 : Total amount of sold drinks over the five years', color=['r'])
    plt.show()

    Drinks.plot.pie(autopct='%.2f', fontsize=10, figsize=(6, 6), title='Extra plot: Percentage of sold drinks over the five years', colors=['c', 'g', 'chartreuse', 'olive', 'paleturquoise', 'mediumspringgreen', 'lightcyan'])
    plt.show()

productFiveYears = False
if productFiveYears:
    df = [Food, Drinks]
    DRINKS_AND_FOOD = pd.concat(df)
    DRINKS_AND_FOOD.plot.pie(autopct='%.2f', fontsize=10, figsize=(6, 6), title='Extra plot: Percentage of sold foods and drinks over the five years', colors=['gold', 'khaki', 'limegreen', 'c', 'g', 'y', 'chartreuse', 'skyblue', 'olive', 'paleturquoise', 'mediumspringgreen', 'lightcyan'])
    plt.show()

print('Preparing Plots: DONE\n')



######## Some buying histories of returning customers
print('Buying histories of 3 returning customers:')
print('----------------------------------------------------')
print('          Customer 1:')
print('----------------------------------------------------')
print(no_more_money[865].budget_history)
print('----------------------------------------------------')
print('          Customer 2:')
print('----------------------------------------------------')
print(no_more_money[56].budget_history)
print('----------------------------------------------------')
print('          Customer 3:')
print('----------------------------------------------------')
print(no_more_money[536].budget_history)
print('----------------------------------------------------')



######## Same Logs as csv-file
print('Saving Logs: ...')
file_name = 'Coffeebar_' + str(date_list[0]) + '_' + str(date_list[-1]) + '.csv'
Logs.to_csv('../Outputs/Simulation/' + file_name, sep=';')
print('Saving Logs: DONE (saved as: "' + file_name + '")\n')

print('Saving Simulation Percentages: ...')
file_name = 'Percentages_Simulation(' + str(date_list[0]) + '_' + str(date_list[-1]) + ').csv'
Percentages_sim.to_csv('../Outputs/Simulation/' + file_name, sep=';')
print('Saving Simulation Percentages: DONE (saved as: "' + file_name + '")')

