import random as rnd



######## General Actions performed with a Client
class Client(object):
    def __init__(self, id_):
        self.id = id_
        self.food_orders = []
        self.drink_orders = []
        self.budget_history = []

    def Does_have_enough_budget(self, min_amount):
        if self.budget >= min_amount:
            return True
        else:
            return False

    def Buy_food(self, product, cost, time):
        if cost > 0:
            self.budget -= cost
            self.food_orders.append({'time': time, 'product': product, 'price': cost})
            self.budget_history.append({'time': time, 'product': product, 'budget': self.budget, 'price': -cost})

    def Buy_drink(self, product, cost, time):
        if cost > 0:
            self.budget -= cost
            self.drink_orders.append({'time': time, 'product': product, 'price': cost})
            self.budget_history.append({'time': time, 'product': product, 'budget': self.budget, 'price': -cost})

    def Give_tip(self, time):
        tip = round(rnd.random() * self.max_tip, 2)
        if tip > 0:
            self.budget -= tip
            self.budget_history.append({'time': time, 'product': 'tip', 'budget': self.budget, 'price': -tip})

        return tip


######## Category Characteristics
class OneTimer(Client):
    def __init__(self, id_):
        Client.__init__(self, id_)
        self.budget = 100
        self.recurrence = 'once'


class Returning(Client):
    def __init__(self, id_):
        Client.__init__(self, id_)
        self.max_tip = 0
        self.recurrence = 'return'


######## Client Type Classes


# Once-Regular
class OnceRegular(OneTimer):
    def __init__(self, id_):
        OneTimer.__init__(self, id_)
        self.type = 'regular'
        self.max_tip = 0
        self.budget_history.append({'time': '', 'product': 'start budget', 'budget': self.budget, 'price': ''})


# Once-TripAdvisor
class OnceTripAdvisor(OneTimer):
    def __init__(self, id_):
        OneTimer.__init__(self, id_)
        self.type = 'trip-advisor'
        self.max_tip = 10
        self.budget_history.append({'time': '', 'product': 'start budget', 'budget': self.budget, 'price': ''})


# Returning-Regular
class ReturningRegular(Returning):
    def __init__(self, id_):
        Returning.__init__(self, id_)
        self.budget = 250
        self.type = 'regular'
        self.budget_history.append({'time': '', 'product': 'start budget', 'budget': self.budget, 'price': ''})


# Returning-Hipster
class ReturningHipster(Returning):
    def __init__(self, id_):
        Returning.__init__(self, id_)
        self.budget = 500
        self.type = 'hipster'
        self.budget_history.append({'time': '', 'product': 'start budget', 'budget': self.budget, 'price': ''})

'''
# Examples:
John = OnceRegular('CID2399097')
John.Buy_drink('Coffee', 2, '16:44:00')

Mitchel = OnceTripAdvisor('CID2398057')
Mitchel.Buy_drink('Tea', 1, '08:05:00')
Mitchel.Buy_food('Muffin', 5, '08:05:00')

Jan = ReturningRegular('CID2699098')
Jan.Buy_drink('Water', 1, '09:50:00')
Jan.Buy_drink('Soda', 3, '12:04:00')
Jan.Buy_food('Sandwich', 7, '12:04:00')

Brian = ReturningHipster('CID2199193')
Brian.Buy_drink('Frappucino', 4, '10:35:00')
Brian.Buy_drink('Milkshake', 5, '14:32:00')
Brian.Buy_drink('Milkshake', 5, '17:20:00')


print(John.budget)
print(John.budget_history)

print(Mitchel.budget)
print(Mitchel.budget_history)

print(Jan.budget)
print(Jan.budget_history)

print(Brian.budget)
print(Brian.budget_history)
'''