import pandas as pd
import numpy  as np
import random as rnd
import matplotlib.pyplot as plt




######## Import Data
Coffeebar = pd.read_csv("../Inputs/CoffeeBar_2013-2017.csv", sep=";")
Coffeebar = Coffeebar.fillna(value="No order")


######## Kinds of food and drinks sold by the coffee bar and number of unique customers
print('Sold Foods:     ' ,  ', '.join([str(x).title()  for x in Coffeebar["FOOD"   ].unique() ]))
print('Sold Drinks:    ' ,  ', '.join([str(x).title()  for x in Coffeebar["DRINKS" ].unique() ]))
print('Unique Clients: ' ,  Coffeebar["CUSTOMER"].nunique())



######## 2 bar plots with the total amount of sold foods (plot1) and drinks (plot2) over the five years
foodFiveYears = True
if foodFiveYears:
    Food = Coffeebar['FOOD'].value_counts()

    Food.plot.bar(title='plot1 : Total amount of sold foods over the five years', color=['lightcoral'])
    plt.show()

    Food.plot.pie(autopct='%.2f', fontsize=10, figsize=(6, 6), title='Extra plot: Percentage of sold food over the five years', colors=['plum', 'blueviolet', 'deeppink', 'dodgerblue', 'm'])
    plt.show()

drinksFiveYears = True
if drinksFiveYears:
    Drinks = Coffeebar['DRINKS'].value_counts()

    Drinks.plot.bar(title='plot2 : Total amount of sold drinks over the five years', color=['indianred'])
    plt.show()

    Drinks.plot.pie(autopct='%.2f', fontsize=10, figsize=(6, 6), title='Extra plot: Percentage of sold drinks over the five years', colors=['c', 'g', 'chartreuse', 'olive', 'paleturquoise', 'mediumspringgreen', 'lightcyan'])
    plt.show()



######## Average that a customer buys a certain food or drink at any given time
Coffeebar['HOURS'] = Coffeebar['TIME'].str[11:19]
Coffeebar['DATES'] = Coffeebar['TIME'].str[ 0:10]

Number_unique_date = Coffeebar['DATES'].nunique()

Percentages_drinks = Coffeebar[['HOURS','DRINKS']].groupby(['HOURS', 'DRINKS']).size().unstack()/ Number_unique_date #* 100
Percentages_food   = Coffeebar[['HOURS', 'FOOD']].groupby(['HOURS', 'FOOD']).size().unstack()/ Number_unique_date #* 100

frames = [Percentages_drinks, Percentages_food]
Percentages = pd.concat(frames, axis=1)

Percentages['HOURS'] = Percentages.index
Percentages = Percentages.fillna(value=0)



######## Saving all percentages
Percentages.to_csv('../Outputs/Exploratory/probabilities_Part_1.csv', sep=';')

# Saving existing unique ID's
f = open('../Outputs/Exploratory/uniqueCID.csv' , 'w')
f.write('\n'.join(list(Coffeebar['CUSTOMER'].unique())))
f.close()


######## Creating Available Client ID's
print('\nGenerating Available CIDs: ...')
existingCIDs = [x.replace('\n', '') for x in list(Coffeebar["CUSTOMER"].unique())]
existing_ID_nums = [int(x.replace('CID', '')) for x in existingCIDs]

allID_nums = np.arange(1, 1E8)
completed_existing_nums = np.zeros(len(allID_nums))

for index, number in enumerate(existing_ID_nums):
    completed_existing_nums[int(number)-1] = int(number)

result = allID_nums - completed_existing_nums
available_ID_nums = allID_nums[result > 0]

num_generating = int(5 * 366 * 171) #20 * len(existing_ID_nums)
print ('Randomizing Set (' + str(num_generating) +" ID's): ...")


selected_available_IDs = []
for i in range(num_generating):
    selected_available_IDs.append( available_ID_nums[rnd.randint(0, len(available_ID_nums)-1)] )

print('Formatting Available IDs: ...')

available_IDs = ['CLIENT_ID']
count = 0
for number in selected_available_IDs:
    number = str(int(number))
    available_IDs.append('CID' + (8 - len(number)) * '0' + number)

f = open('../Outputs/Exploratory/Available_IDs.csv', 'w')
f.write('\n'.join(available_IDs))
f.close()

print('DONE.')